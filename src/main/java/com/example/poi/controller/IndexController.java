package com.example.poi.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.example.poi.dto.ExcelDTO;
import com.example.poi.service.PoiService;

@Controller
public class IndexController {
	
	private static final Logger logger = LoggerFactory.getLogger(IndexController.class);

	@Autowired private PoiService poiService = null; 
	
	/**
	 * @writer iwntutovibrate
	 * @param model
	 * @throws IOException
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String index(Model model) throws IOException {
		
		logger.info("Come index .");
		
		File file = new File("ExcelFilePath.xls");
		FileInputStream excelFileInputStream = new FileInputStream(file);
		
		Workbook workbook = new HSSFWorkbook(excelFileInputStream);
		
		/**
		 * if you want sheet number.
		 * workbook.getSheetAt(here);
		 */
		Sheet sheet = workbook.getSheetAt(0);
		
		List<ExcelDTO> excelList = poiService.convertExcel(sheet);
		excelFileInputStream.close();
		
		model.addAttribute("excelList", excelList);
		
		return "index";
	}
	
}
