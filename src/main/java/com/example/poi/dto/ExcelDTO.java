package com.example.poi.dto;

public class ExcelDTO {

	/**
	 * Excel Data
	 */
	
	private String excelData = null;

	public String getExcelData() {
		return excelData;
	}

	public void setExcelData(String excelData) {
		this.excelData = excelData;
	}

	@Override
	public String toString() {
		return "ExcelDTO [excelData=" + excelData + "]";
	}
	
}
