package com.example.poi.service;

import java.util.LinkedList;
import java.util.List;

import org.apache.poi.ss.usermodel.Sheet;
import org.springframework.stereotype.Service;

import com.example.poi.dto.ExcelDTO;
import com.example.poi.utill.PoiUtill;

@Service
public class PoiService {

	/**
	 * @writer iwntutovibrate
	 * @param sheet
	 */
	public List<ExcelDTO> convertExcel(Sheet sheet){
		
		List<ExcelDTO> list = new LinkedList<>();
		int rowCnt = sheet.getPhysicalNumberOfRows();
		
		// here you need cell number
		int cellNumber = 0; 
		
		for (int i = 0; i < rowCnt; i++) {
			
			ExcelDTO excelDTO = new ExcelDTO();
			excelDTO.setExcelData(PoiUtill.getCellValue(sheet, i, cellNumber));
			
			list.add(excelDTO);
		}
		
		return list;
	}
}
