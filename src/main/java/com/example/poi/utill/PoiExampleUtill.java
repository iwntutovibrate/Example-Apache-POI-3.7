package com.example.poi.utill;

import java.io.FileInputStream;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

public class PoiExampleUtill {

	HSSFRow row;
	HSSFCell cell;
	
	public void getCellValue() {
		
		try {
			String filePath = "excelFilePathHere";
			FileInputStream inputStream = new FileInputStream(filePath);
			POIFSFileSystem fileSystem = new POIFSFileSystem(inputStream);
			HSSFWorkbook workbook = new HSSFWorkbook(fileSystem);

			int sheetCn = workbook.getNumberOfSheets();
			System.out.println("Count sheets : " + sheetCn);

			for(int cn = 0; cn < sheetCn; cn++){
				
				System.out.println("Sheet Name : " + workbook.getSheetName(cn));

				HSSFSheet sheet = workbook.getSheetAt(cn);

				int rows = sheet.getPhysicalNumberOfRows();
				System.out.println("Count sheet rows : " + rows);

				int cells = sheet.getRow(cn).getPhysicalNumberOfCells(); //
				System.out.println(" Count sheet cells : " + cells);

				for (int r = 0; r < rows; r++) {
					
					row = sheet.getRow(r);
					
					if (row != null) {
						
						for (int c = 0; c < cells; c++) {

							cell = row.getCell(c);

							if (cell != null) {

								String value = null;
								switch (cell.getCellType()) {
								
								case HSSFCell.CELL_TYPE_FORMULA:
									value = cell.getCellFormula();
									break;

								case HSSFCell.CELL_TYPE_NUMERIC:
									value = "" + cell.getNumericCellValue();
									break;

								case HSSFCell.CELL_TYPE_STRING:
									value = "" + cell.getStringCellValue();
									break;

								case HSSFCell.CELL_TYPE_BLANK:
									value = "" + cell.getBooleanCellValue();
									break;

								case HSSFCell.CELL_TYPE_ERROR:
									value = "" + cell.getErrorCellValue();
									break;

								default:
									value = "[type?]";
								}
								System.out.print(value + "\t");

							} else {
								System.out.print("[null]\t");
							}
						} 
						System.out.print("\n");
					}
					
				}
				System.out.println(workbook.getSheetName(cn) + " sheet end");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
