package com.example.poi.utill;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

public class PoiUtill {

	/**
	 * @writer iwntutovibrate
	 * @param sheet
	 * @param rownum
	 * @param cellnum
	 */
	public static String getCellValue(Sheet sheet, int rownum, int cellnum) {

		Row row = sheet.getRow(rownum);
		String value = "";
		
		if (row != null) {

			Cell cell = row.getCell(cellnum);
			if (cell != null) {

				try {
					
					value = cell.getRichStringCellValue().toString();
					
				} catch (Exception e) {
					
					if (DateUtil.isCellDateFormatted(cell)) {
						
						Date date = cell.getDateCellValue();
						value = new SimpleDateFormat("yyyy-MM-dd").format(date);
						
					} else {
						
						value = String.valueOf(cell.getNumericCellValue());
					}
				}
			}
		}
		return value;
	}
	
}
